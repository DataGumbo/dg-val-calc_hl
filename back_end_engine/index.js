// import dependencies
const express = require('express')
var cors = require('cors')
const app = express()
app.use(cors())
const port = 3000
const db = require('monk')('localhost/analysisDB')


//JSON parser
app.use(express.json())

//ANALYSIS DATA 
//Retrieve data from server (from MongoDB)
app.get('/inputData', async (req, res) => {
    const inputData = await db.get("inputData").find({})
    res.send(inputData)
})

app.get('/inputData/:analysisID', async (req, res) => {
    const analysisID = req.params.analysisID
    const inputData = await db.get("inputData").find({"analysisID": analysisID})
    res.send(inputData)
})

// Send and receive feedback of what is sent
app.post('/inputData', async (req, res) => {

    const input = req.body
    console.log("input in body", input)
    const result = await
        db.get("inputData").insert(input)
    res.send(result)
})


//FORMULAS
app.get('/analyses', async (req, res) => {

    const analyses = await db.get("analyses").find({})
    res.send(analyses)
})

// endpoint to read analysis from Mongo DB
app.get('/analyses/:analysisID', async (req, res) => {

    const analysisID = req.params.analysisID;
    const analysis = await db.get("analyses").findOne({_id: analysisID})
    
    res.send(analysis)
})

// endpoint to create a analysis
app.post('/analyses', async (req, res) => {

    const input = req.body
    console.log("insert analysis", input)
    const result = await
        db.get("analyses").insert(input)

    res.send(result)
})

// update a analysis
app.put('/analyses/:analysisID', async (req, res) => {

    const input = req.body
    const analysisID = req.params.analysisID;
    const result = await
        db.get("analyses").update({_id: analysisID}, {$set: input})
    res.send(result)
})


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})




