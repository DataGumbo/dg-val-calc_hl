import React, { useState, useEffect } from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import { useLocation } from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import OverviewContainer from './containers/OverviewContainer'
import EngineDetailsContainer from './containers/EngineDetailsContainer'
import Footer from './components/Footer'

import calcService from './services/CalculatorService'
// Logos
import dgLogoDark from './assets/images/datagumbo/dgPic.png'
import dgLogowhite from './assets/images/datagumbo/dgPic.png'

// Bootstrap and plugins CSS
import './assets/css/bootstrap.min.css'
import './assets/css/font-awesome.min.css'

// Custom CSS
import './assets/css/green.css'


let calcData = {}

function App() {
    const [engineContent, setEngineContent] = useState([])

    const [showToolBar, setShowToolBar] = useState(false)

    const [toSave, setToSave] = useState(false)

    const [showModal, setShowModal] = useState(false)

    // const [toTest, setToTest] = useState(false)

    const [formulaToTest, setFormulaToTest] = useState({})

    const location = useLocation()

    const handleOnChange = (data) => {
        calcData = data;

        console.log("handleOnChange: ", calcData);

        if(calcData.analysisInputVals){
            console.log("there is data change in the table :: ", calcData.analysisInputVals);

        }

    }

    useEffect(() => {
        (async () => {

        })()
    }, [])

    useEffect(() => {
        (async () => {
            if (toSave) {
                var curTimeUTC = new Date();
                calcData['UTCTimeStamp'] = curTimeUTC;
                console.log("calling save...", calcData)
                const analysisInputVals = calcData.analysisInputVals
                delete calcData.analysisInputVals
                const analysis = await calcService.saveAnalysis(calcData)
                console.log("this is calcData : ", calcData);
                console.log("this is analysisInputVals : ", analysisInputVals);
                if (analysisInputVals) {
                    for (const cf of analysisInputVals) {
                        cf.analysisID = analysis._id
                    }
                    await calcService.saveAnalysisData(analysisInputVals)
                }
                console.log("save successfully")
                calcData = {}
            } else {
                console.log("not save...", calcData)
            }
        })()

        return function cleanup() {
            console.log("cleanup detail data:", calcData)

            setToSave(false)
        };
    }, [toSave])

    // useEffect(() => {
    //     (async () => {
    //         if (toTest) {
    //             const analyseID = location.state?.analysisID
    //             if ( analyseID ) {
    //                 const analysis = await calcService.getAnalysis(analyseID)
    //                 setFormulaToTest(analysis)

    //             }
    //         } else {
    //             console.log("not test...")
    //         }
    //     })()

    //     return function cleanup() {
    //         setToTest(false)
    //     };
    // }, [toTest])

    return (
        <div>
            <header className="header-area">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <nav className="main-nav">
                                <Link to="/" className="logo">
                                    <img
                                        src={dgLogowhite}
                                        className="light-logo"
                                        alt="Data Gumbo Corporation"
                                        style={{ height: '50px', marginTop: '-15px' }}
                                    />
                                    <img
                                        src={dgLogoDark}
                                        className="dark-logo"
                                        alt="Data Gumbo Corporation"
                                        style={{ height: '50px', marginTop: '-15px' }}
                                    />
                                </Link>

                                <ul className="nav">
                                    <li>
                                        <Link to="/">OVERVIEW</Link>
                                    </li>
                                    {showToolBar === true && (
                                        <>
                                            <li>
                                                <a
                                                    href="javascript:void(0)"
                                                    className="btn-nav-box"
                                                    onClick={() => {
                                                        alert('New Analysis is successfully saved!');
                                                        setToSave(true);

                                                        window.setTimeout(() => {
                                                            window.location.href = '/';
                                                        }, 1500
                                                        );
                                                    }}
                                                >
                                                    SAVE
                                                </a>
                                            </li>
                                            {/* <li>
                              <a
                                href="javascript:void(0)"
                                className="btn-nav-box"
                                onClick={()=>{
                                  setToTest(true)
                                  setShowModal(true)
                                  
                                }}
                              >
                              TEST
                              </a>
                          </li> */}
                                        </>
                                    )}
                                </ul>
                                <a className="menu-trigger">
                                    <span>Menu</span>
                                </a>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>

            <Switch>
                <Route path="/calculator-details">
                    <EngineDetailsContainer setShowToolBar={setShowToolBar} onChange={handleOnChange} />
                    <Footer />
                </Route>
                <Route path="/">
                    <OverviewContainer engineContent={engineContent} />
                    <Footer />
                </Route>
            </Switch>

            <MyVerticallyCenteredModal
                key={new Date().getTime()}
                show={showModal}
                onHide={() => setShowModal(false)}
                analysis={formulaToTest}
            />
        </div>
    )
}



function MyVerticallyCenteredModal(props) {
    const analysis = props.analysis

    const [calcPayload, setCalcPayload] = useState({})
    const [result, setResult] = useState([])

    calcPayload.analysisID = analysis?._id

    const getOptions = (dropdowns, keys, variable) => {
        const keyIndex = keys.indexOf(variable.name)
        let node = dropdowns
        let children = [<option>-- Select --</option>]
        for (let i = 0; i <= keyIndex; i++) {
            children = node[keys[i]]
            if (i < keyIndex) {
                const filtered = children.filter(node => node.val === calcPayload[keys[i]])
                if (filtered && filtered.length > 0) {
                    node = filtered[0]
                }
            }
        }

        if (!children) return [<option>-- Select --</option>]

        return [<option>-- Select --</option>].concat(children.map(child => {
            return <option>{child.val}</option>
        }))
    }

    const renderInputOrSelect = (analysis, variable) => {

        if (analysis && analysis.dropdowns) {
            const dropdowns = analysis.dropdowns
            const keys = dropdowns.keys
            const keyIndex = keys.indexOf(variable.name)
            if (keyIndex >= 0) {
                return (
                    <Form.Control as="select" onChange={(e) => {
                        calcPayload[variable.name] = e.target.value
                        setCalcPayload({ ...calcPayload })
                    }}>
                        {getOptions(dropdowns, keys, variable)}
                    </Form.Control>
                )

            } else {
                return (
                    <input
                        type="text"
                        placeholder={'Enter value'}
                        onChange={(e) => {
                            calcPayload[variable.name] = e.target.value;
                        }}
                        value={calcPayload[variable.name]}
                    />
                )
            }
        } else {
            return (<input
                type="text"
                placeholder={'Enter value'}
                onChange={(e) => {
                    calcPayload[variable.name] = e.target.value;
                }}
                value={calcPayload[variable.name]}
            />)
        }
    }

    const sortInput = (analysis) => {
        const lookups = []
        const inputs = []
        for (const variable of analysis.variables) {
            if (analysis && analysis.dropdowns) {
                const keys = analysis.dropdowns.keys
                const keyIndex = keys.indexOf(variable.name)
                if (keyIndex >= 0) {
                    lookups.push(variable)
                } else {
                    inputs.push(variable)
                }
            } else {
                inputs.push(variable)
            }
        }
        return lookups.concat(inputs)
    }

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {analysis?.name}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="container">
                    <div className="row m-bottom-60">
                        <div className="col-lg-12">
                            <p>
                                {analysis?.description}
                            </p> <br />
                            <div className="table-responsive">
                                <table className="table table-striped table-latests table-detail">
                                    <tbody>
                                        {
                                            analysis && analysis.variables &&
                                            sortInput(analysis).filter(v => v.type === "Input").map(v => {
                                                return (
                                                    <tr>
                                                        <td style={{ verticalAlign: 'middle' }}>
                                                            <strong> {v.label ? v.label : v.name} {v.unit && (<span>({v.unit})</span>)} </strong>
                                                        </td>

                                                        <td>
                                                            <div className="calculator-input">
                                                                <div className="input-wrapper">
                                                                    <div className="input">
                                                                        {renderInputOrSelect(analysis, v)}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                {(result && result.length > 0) &&
                                    (<div style={{ fontWeight: 'bold' }}>Results:</div>)
                                }
                                {
                                    (result && result.length > 0) && result.map(r => {
                                        return <div>
                                            <span>{r.expName} = </span>
                                            <span style={{ fontSize: '18px' }}>{new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 5 }).format(r.result)}</span>
                                            <span style={{ paddingLeft: '10px' }}>{r.unit}</span>
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>

            </Modal.Body>
            <Modal.Footer>
                <Button style={{ width: '200px' }} onClick={async () => {
                    const result = await calcService.calculateFormula(calcPayload)
                    setResult(result)
                }}>Calculate</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default App
