import React, { useState, useRef, useEffect } from 'react'
import { useLocation } from 'react-router-dom'

import Plot from 'react-plotly.js';
import plotService from '../services/PlotingService'

import jspreadsheet from "jspreadsheet-ce";
import calcService from '../services/CalculatorService'
import "../../node_modules/jspreadsheet-ce/dist/jspreadsheet.css";

import 'react-responsive-modal/styles.css'

let data = {

}


// function init(){}


const toArray = (jsonArray) => {
    const array = []
    let first = true
    const header = []
    for (const jsonObj of jsonArray) {
        if (first) {
            for (const prop in jsonObj) {
                if (prop === "analysisID" || prop === "_id") continue
                header.push(prop)
            }
            array.push(header)
            first = false
        }
        const row = []
        for (const prop of header) {
            row.push(jsonObj[prop])
        }
        array.push(row)
    }
    return array
}

function EngineDetailsContainer({ setShowToolBar, onChange }) {

    const jRef = useRef(null);
    
    const options = {
        data: [[]],
        minDimensions: [5, 5],
        columns: [
            { width: 110 },
            { width: 110 },
            { width: 100 },
            { width: 100 },
            { width: 100 },
            { width: 100 },
            { width: 100 },
            { width: 100 },
            { width: 100 },
            { width: 100 }
        ],
        onafterchanges: () => {
            console.log("onafterchanges...")
            const jsonArray = [];
            const excelData = jRef.current.jexcel.getData();
            console.log("this is excel data :: ", excelData);
            var totalCol = excelData.filter(Boolean).length;
            if (excelData && totalCol > 0) {
                console.log("this is length of row :: ", totalCol);

                // create the header, also the key of values in each columns
                const row0 = excelData[0] //header

                // loop to get all values in each columns
                for (let row = 1; row < totalCol; row++) {
                    const obj = {}
                    for (let col = 0; col < excelData[row].filter(Boolean).length; col++) {
                        obj[row0[col]] = excelData[row][col]
                    }
                    jsonArray.push(obj)
                }
            }
            data.analysisInputVals = jsonArray;
            onChange(data);
        }
    };

    // const [equation, setEquation] = useState("");
    // const [equation2, setEquation2] = useState("");
    const [name, setName] = useState("");
    const [currency, setCurrency] = useState("");
    const [analysisType, setAnalysisType] = useState("");
    const [plotData, setPlotData] = useState([]);

    // const [expUnit, setExpUnit] = useState("");

    // const [expName2, setExpName2] = useState("");
    // const [expUnit2, setExpUnit2] = useState("");

    const [desc, setDescription] = useState("");
    // const [variables, setVariables] = useState([]);

    const location = useLocation()

    useEffect(() => {
        console.log("cleaning data:", data)
        data = {}
        const analyseID = location.state?.analysisID
        console.log("analysisID:", analyseID)
        if (analyseID) {
            (async () => {
                const analysis = await calcService.getAnalysis(analyseID);
                const inputData = await calcService.getAnalysisData(analyseID);
                const chartData = await plotService.plotAnalysis(inputData);
                setPlotData(chartData);
                setName(analysis.name);
                setDescription(analysis.description);
                setCurrency(analysis.currency);
                setAnalysisType(analysis.analysisType);
                jRef.current.jexcel.setData(toArray(inputData));
            })();
        }
    }, []);

    useEffect(() => {
        setShowToolBar(true)
        return function cleanup() {
            setShowToolBar(false)
        };
    }, []);

    useEffect(() => {
        if (!jRef.current.jspreadsheet) {
            jspreadsheet(jRef.current, options);
        }
    }, [options]);

    console.log("this is data for plot ::", plotData);
    return (
        <>
            <section
                className="engine-explorer-wrapper bg-bottom-center"
                id="welcome-1"
            >
                <div className="engine-explorer text">
                    <div className="container text-center">
                        <div className="row">
                            <div className="col-lg-12 align-self-center">
                                <h1  style={{marginTop:'50px'}}>Value Proposition Calculator</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="engine-explorer-section section bg-bottom">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="center-heading">
                                <h2 className="section-title">Analysis MetaData</h2>
                            </div>
                        </div>
                        <div className="offset-lg-3 col-lg-6 m-bottom-30"></div>
                    </div>
                    <div className="row m-bottom-60">
                        <div className="col-lg-12 col-md-12 col-sm-12">
                            <div className="table-responsive">
                                <table className="table table-striped table-latests table-detail">
                                    <tbody>
                                        <tr>
                                            <td style={{ verticalAlign: 'middle' }}>
                                                <strong>Analysis Name</strong>
                                            </td>
                                            <td colSpan={3}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="Enter a name for your analysis"
                                                                onChange={(e) => {
                                                                    data.name = e.target.value
                                                                    setName(data.name)
                                                                    onChange(data)
                                                                }}
                                                                value={name}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style={{ verticalAlign: 'middle' }}>
                                                <strong>Description</strong>
                                            </td>
                                            <td colSpan={3}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="Description"
                                                                onChange={(e) => {
                                                                    data.description = e.target.value
                                                                    setDescription(data.description)
                                                                    onChange(data)
                                                                }}
                                                                value={desc}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style={{ verticalAlign: 'middle' }}>
                                                <strong>Unit of Currency</strong>
                                            </td>
                                            <td colSpan={3}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="For example: USD, NOK, CAD, EU"
                                                                value={currency}
                                                                onChange={
                                                                    (e) => {
                                                                        data.currency = e.target.value
                                                                        setCurrency(e.target.value)
                                                                        onChange(data)
                                                                    }
                                                                }
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            {/* <td>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="Math Expression"
                                                                value={equation}
                                                                onChange={
                                                                    (e) => {
                                                                        data.expression = e.target.value
                                                                        setEquation(e.target.value)
                                                                        onChange(data)
                                                                        try {
                                                                            const node = parse(e.target.value)
                                                                            data.variables = getVariables(node);
                                                                            setVariables(data.variables)
                                                                            onChange(data)
                                                                        }
                                                                        catch (e) {
                                                                            console.error(e)
                                                                        }
                                                                    }
                                                                }
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td> */}
                                            {/* <td width={200}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <select
                                                                type="text"
                                                                placeholder="Unit"
                                                                onChange={(e) => {
                                                                    data.expUnit = e.target.value
                                                                    setExpUnit(e.target.value)
                                                                    onChange(data)
                                                                }}
                                                                value={expUnit}
                                                            >
                                                                <option>None</option>
                                                                <option>Short ton</option>
                                                                <option>Gallon</option>
                                                                <option>kg CO2 </option>
                                                                <option>g N2O </option>
                                                                <option>kg CO2 per mmBtu</option>
                                                                <option>kg CO2 per gallon</option>
                                                                <option>g N2O per mmBtu</option>
                                                                <option>mmBTU per short ton</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>*/}
                                        </tr>
                                        <tr>
                                            <td style={{ verticalAlign: 'middle' }}>
                                                <strong>Analysis Type</strong>
                                            </td>
                                            <td colSpan={3}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="For example: Spending, Estimating, Run Date"
                                                                value={analysisType}
                                                                onChange={
                                                                    (e) => {
                                                                        data.analysisType = e.target.value
                                                                        setAnalysisType(e.target.value)
                                                                        onChange(data)
                                                                    }
                                                                }
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        {/* <tr>
                                            <td style={{ verticalAlign: 'middle' }}>
                                                <strong>Math Expression 2</strong>
                                            </td>
                                            <td width={200}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="Exp Name"
                                                                value={expName2}
                                                                onChange={
                                                                    (e) => {
                                                                        data.expName2 = e.target.value
                                                                        setExpName2(e.target.value)
                                                                        onChange(data)
                                                                    }
                                                                }
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <input
                                                                type="text"
                                                                placeholder="Math Expression"
                                                                value={equation2}
                                                                onChange={
                                                                    (e) => {
                                                                        data.expression2 = e.target.value
                                                                        setEquation2(e.target.value)
                                                                        onChange(data)
                                                                        try {
                                                                            const node = parse(e.target.value)
                                                                            const vars = getVariables(node);
                                                                            const unique = []
                                                                            for (const v of vars) {
                                                                                if (!variableExists(variables, v.name)) {
                                                                                    unique.push(v)
                                                                                }
                                                                            }
                                                                            data.variables = [...variables, ...unique]//getVariables(node);
                                                                            setVariables(data.variables)
                                                                            onChange(data)
                                                                        }
                                                                        catch (e) {
                                                                            console.error(e)
                                                                        }
                                                                    }
                                                                }
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td width={200}>
                                                <div className="calculator-input">
                                                    <div className="input-wrapper">
                                                        <div className="input">
                                                            <select
                                                                type="text"
                                                                placeholder="Unit"
                                                                onChange={(e) => {
                                                                    data.expUnit2 = e.target.value
                                                                    setExpUnit2(e.target.value)
                                                                    onChange(data)
                                                                }}
                                                                value={expUnit2}
                                                            >
                                                                <option>None</option>
                                                                <option>Short ton</option>
                                                                <option>Gallon</option>
                                                                <option>kg CO2 </option>
                                                                <option>g N2O </option>
                                                                <option>kg CO2 per mmBtu</option>
                                                                <option>kg CO2 per gallon</option>
                                                                <option>g N2O per mmBtu</option>
                                                                <option>mmBTU per short ton</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> */}

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    {/* <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="center-heading">
                                    <h2 className="section-title">Variables</h2>
                                </div>
                            </div>
                            <div className="offset-lg-3 col-lg-6 m-bottom-30" />
                        </div>
                        <div className="row m-bottom-60">
                            <div className="col-lg-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-latests table-detail">
                                        <tbody>
                                            {
                                                variables.map(v => {
                                                    return (
                                                        <tr>
                                                            <td style={{ verticalAlign: 'middle' }}>
                                                                <strong> {v.name} </strong>
                                                            </td>
                                                            <td>
                                                                <div className="calculator-input">
                                                                    <div className="input-wrapper">
                                                                        <div className="input">
                                                                            <select
                                                                                type="text"
                                                                                placeholder="Math Expression"
                                                                                onChange={(e) => {
                                                                                    v.type = e.target.value
                                                                                    const clone = [...variables]
                                                                                    setVariables(clone)
                                                                                    onChange(data)
                                                                                }}
                                                                                value={v.type}
                                                                            >
                                                                                <option>Input</option>
                                                                                <option>Lookup</option>
                                                                                <option>Rule</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="calculator-input">
                                                                    <div className="input-wrapper">
                                                                        <div className="input">
                                                                            <input
                                                                                type="text"
                                                                                placeholder={v.type === "Input" ? "Label" : (v.type === "Lookup" ? "Lookup keys" : "Condition and Conclusion")}
                                                                                onChange={(e) => {
                                                                                    v.label = e.target.value
                                                                                    if (v.type === "Lookup") {
                                                                                        const value = e.target.value
                                                                                        const vars = value.split(",")
                                                                                        for (let v of vars) {
                                                                                            v = v.trim()
                                                                                            if (v !== "") {
                                                                                                let found = variableExists(variables, v)
                                                                                                if (found === false) {
                                                                                                    console.log("adding var: ", v)
                                                                                                    data.variables.push({
                                                                                                        name: v,
                                                                                                        type: "Input"
                                                                                                    })
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        const clone = [...data.variables]
                                                                                        setVariables(clone)

                                                                                        onChange(data)
                                                                                    } else {
                                                                                        const clone = [...data.variables]
                                                                                        setVariables(clone)
                                                                                        onChange(data)
                                                                                    }
                                                                                }}
                                                                                value={v.label}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="calculator-input">
                                                                    <div className="input-wrapper">
                                                                        <div className="input">
                                                                            <select
                                                                                type="text"
                                                                                placeholder="Unit"
                                                                                onChange={(e) => {
                                                                                    v.unit = e.target.value
                                                                                    data.variables = variables
                                                                                    const clone = [...data.variables]
                                                                                    setVariables(clone)
                                                                                    onChange(data)
                                                                                }}
                                                                                value={v.unit}
                                                                            >
                                                                                <option>None</option>
                                                                                <option>Short ton</option>
                                                                                <option>Gallon</option>
                                                                                <option>kg CO2 </option>
                                                                                <option>g N2O </option>
                                                                                <option>kg CO2 per mmBtu</option>
                                                                                <option>kg CO2 per gallon</option>
                                                                                <option>g N2O per mmBtu</option>
                                                                                <option>mmBTU per short ton</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> */}

                    <div className="container">
                        {/* HEADING OF THE TABLE */}
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="center-heading">
                                    <h2 className="section-title">Analysis Data</h2>
                                </div>
                            </div>
                            <div className="offset-lg-3 col-lg-6 m-bottom-30" />
                        </div>
                        {/* FOR THE TABLE FIELDS */}
                        <div className="row m-bottom-60">
                            <div className="col-lg-12">
                                <div>
                                    <div ref={jRef} />
                                </div>
                            </div>
                        </div>


                        {/*FOR THE PLOTLY  */}
                        <div className="row m-bottom-60">
                            <div className="col-lg-12">
                                <div>
                                <Plot data={plotData[0]} layout={plotData[1]} />
                                </div>
                            </div>
                        </div>



                        
                    </div>
                </div>
            </section>

        </>
    )
}

export default EngineDetailsContainer
