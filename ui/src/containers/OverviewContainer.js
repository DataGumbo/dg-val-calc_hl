import React, {useState, useEffect} from 'react'
import { useHistory } from "react-router-dom";

import calcService from '../services/CalculatorService'
import './container.css'

function OverviewContainer({ engineContent }) {
  const history = useHistory();
  const navigateTo = (analysisID) => {
    if ( analysisID ) {
      history.push( {
          pathname: '/calculator-details/'+analysisID,
          state: { analysisID: analysisID}
      });
    } else {
      history.push('/calculator-details');
    }
  }

  const [calculators, setCalculators] = useState([])

  useEffect(() => {
    (async () => {
      const analyses = await calcService.getAnalyses()
      setCalculators(analyses)
    })()
  }, [])

  return (
    <>
      <section
        className="engine-explorer-wrapper bg-bottom-center"
        id="welcome-1"
      >
        <div className="engine-explorer text">
          <div className="container text-center">
            <div className="row">
              <div className="col-lg-12 align-self-center">
                <h1 style={{marginTop:'60px'}}>Value Proposition Calculators</h1>
                <span style={{fontSize:'20px', color: 'white', fontWeight:'400', fontStyle:"italic"}}>Where true values revealed</span>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="engine-explorer-features section bg-bottom">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="center-heading">
                <h2 className="section-title">Analysis Library</h2>
              </div>
            </div>
            <div className="offset-lg-3 col-lg-6 m-bottom-30" />
          </div>
          <div className="row">
            { calculators.map( c => {
                return (<div className="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div className="item calculator" onClick={ () => {
                    navigateTo(c._id)
                }}>
                  <div className="title">
                    <div className="" />
                    <h5>{c.name}</h5>
                  </div>
                  <div className="text">
                    <span>{c.description}</span>
                  </div>
                </div>
              </div>)
              })
            }
            <div className="col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div className="item calculator" onClick={ () => {
                  navigateTo()
              }}>
                <div className="title">
                  <div className="" />
                  <h5>New</h5>
                </div>
                <div className="text">
                  <span style={{fontSize: '35px'}}>
                    {'+'}
                  </span>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>
    </>
  )
}

export default OverviewContainer
