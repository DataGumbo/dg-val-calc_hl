class PlottingService{
    async plotAnalysis(inputTableData) {
        const res = await inputTableData;        
        console.log("data for plotting inputTableData:: ", inputTableData);
        
        var monthArr =[];
        inputTableData.forEach(obj => {
            Object.entries(obj).forEach(([k,v]) => {
                if (k==="Month") {
                    monthArr.push(v);
                }
            })
        })
        console.log("this is month :: ", monthArr );


        var buyerValArr =[];
        inputTableData.forEach(obj => {
            Object.entries(obj).forEach(([k,v]) => {
                if (k==="Buyer Value") {
                    buyerValArr.push(v);
                }
            })
        })
        console.log("this is buyer Val :: ", buyerValArr );


        var sellerValArr =[];
        inputTableData.forEach(obj => {
            Object.entries(obj).forEach(([k,v]) => {
                if (k==="Seller Value") {
                    sellerValArr.push(v);
                }
            })
        })
        console.log("this is seller Val :: ", sellerValArr );



        var trace1 = {
            type: "line+marker",
            x: monthArr,
            y: buyerValArr, // slice first 10 data
            name: 'Buyer Value',
        };

        var trace2 = {
            type: "line+marker",
            x: monthArr,
            y: sellerValArr, // slice first 10 data
            name: 'Seller Value',
        };

        var dataForPlot = [trace1, trace2];
    
        // specify layout format parameters
        var layoutOfPlot = {
            title: `Spending Summary`,
            xaxis: {
                title: {
                    text: "Month"
                },
                showgrid : true, // major grid lines 
                showline: true,   // axis line
                rangemode : "tozero"
            },
            yaxis: {
                title: {
                    text: "Spending"
                },  
                // autorange: "reversed",
                showgrid : true,   // major grid lines
                showline: true,    // axis line
                rangemode : "tozero"
            }
        };    
    
        var sumArr = [ dataForPlot, layoutOfPlot ]
        console.log( "sumArr ::", sumArr );

        // return sumArr;
        return  sumArr;
    }
}

const plotService = new PlottingService();

export default plotService