const baseUrl = 'http://localhost:3000'

class CalculatorService {

    async getAnalyses() {
        const res = await fetch(`${baseUrl}/analyses`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
        const result = await res.json()
        return result
    }

    async getAnalysis(analysisID) {
        const res = await fetch(`${baseUrl}/analyses/${analysisID}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
        const result = await res.json()
        return result
    }

    // async calculateFormula(input) {
    //     const res = await fetch(`${baseUrl}/analyses/calculate`, {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json',
    //         },
    //         body: JSON.stringify(input)
    //     })
    //     const result = await res.json()
    //     return result
    // }

    async saveAnalysis(analysis) {

        if ( analysis._id ){
            const res = await fetch(`${baseUrl}/analyses/${analysis._id}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(analysis)
                } )

            const result = await res.json()
            return result
        }
        else {
            const res = await fetch(`${baseUrl}/analyses`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(analysis)
            } )

            const result = await res.json()
            return result
        }
    }

    async getAnalysisData(analysisID) {
        const res = await fetch(`${baseUrl}/inputData/${analysisID}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
        const result = await res.json()
        return result
    }

    async saveAnalysisData(listOfInpVals) {

        const res = await fetch(`${baseUrl}/inputData`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(listOfInpVals)
        } )

        const result = await res.json()
        return result
    }

    async updateInputVals(listOfInpVals) {

        const res = await fetch(`${baseUrl}/inputData`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(listOfInpVals)
        } )

        const result = await res.json()
        return result
    }
}

const calcService = new CalculatorService();

export default calcService