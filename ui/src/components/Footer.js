import React from 'react'

function Footer() {
  return (
    <footer id="contact">
      <div className="footer-bottom slim">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <p className="copyright" style={{marginBottom: '3px', fontSize:'17px'}}>2021 © Data Gumbo Corporation</p>
              <p className="copyright" style = {{fontSize:'12px', color:'red'}}>All Rights Reserved</p>
			
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
